/* 
 * File:   stringSearch.c
 * Author: vagrant
 *
 * Created on February 11, 2015, 6:55 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <search.h>
#include <string.h>

/*
 * 
 */

int compare(const void *fWord, const void *sWord) {
  const char **word1 = (const char **) fWord;
  const char **word2 = (const char **) sWord;

  return strcmp(*word1, *word2);
}

int main(int argc, char** argv) {
  
  int   elementsNum = 6;
  char *names[]     = {"Juan", "Lucia", "Pedro", "Jesus", "Maria", "Antonio"};
  char *key         = "Antonio";
  
  int  result = lfind(&key, names, &elementsNum, sizeof (char **), compare);
  
  if (result == NULL) {
    printf("The word does not exist in the array\n");
    return EXIT_FAILURE;
  } else {
    printf("The word is located in: %p \n", result);
    return EXIT_SUCCESS;
  }

  return (EXIT_SUCCESS);
}


